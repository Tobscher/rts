﻿using UnityEngine;
using System.Collections;

public class Palette {
	public static float Width = 320.0f;
	public static float Height = 240.0f;

	public static float TextureWidth = 16.0f;
	public static float TextureHeight = 16.0f;

	public static float Columns = Width / TextureWidth;
	public static float Rows = Height / TextureHeight;

	public enum Color {
		Red = 1,
		Pink,
		Purple,
		DeepPurple,
		Indigo,
		Blue,
		LightBlue,
		Cyan,
		Teal,
		Green,
		LightGreen,
		Lime,
		Yellow,
		Amber,
		Orange,
		DeepOrange,
		Brown,
		Grey,
		BlueGrey,
	}

	public enum Weight {
		Base = 1,
		_50,
		_100,
		_200,
		_300,
		_400,
		_500,
		_600,
		_700,
		_800,
		_900,
		A100,
		A200,
		A400,
		A700
	}

	private static Vector2 GetColor(int column, int row) {
		var halfWidth = TextureWidth / 2.0f;
		var halfHeight = TextureHeight / 2.0f;

		float startX = (column * TextureWidth) + halfWidth;
		float startY = Height - (row * TextureHeight) - halfHeight;

		return new Vector2(startX / Width, startY / Height);
	}
		
	public static Vector2 GetColor(Color c, Weight w) {
		int column = (int)c - 1;
		int row = (int)w - 1;

		return GetColor(column, row);
	}

	public static Vector2 GetBlack() {
		var column = (int)Columns - 1;
		var first = 1;
		var row = first - 1;

		return GetColor(column, row);
	}

	public static Vector2 GetWhite() {
		var column = (int)Columns - 1;
		var second = 2;
		var row = second - 1;

		return GetColor(column, row);
	}
}