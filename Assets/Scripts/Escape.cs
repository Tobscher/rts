﻿using UnityEngine;
using System.Collections;

public class Escape : MonoBehaviour {
	private HumanPlayerScript humanPlayerScript;
	
	void Start () {
		var humanPlayer = GameObject.FindGameObjectWithTag("HumanPlayer");
		humanPlayerScript = humanPlayer.GetComponent<HumanPlayerScript>();
	}

	public void Update() {
		if (humanPlayerScript == null) {
			return;
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			humanPlayerScript.UnselectAll();
		}
	}
}