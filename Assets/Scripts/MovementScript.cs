﻿using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour {
	private NavMeshAgent agent;
	private GameObject moveTarget;


	void Start() {
		agent = GetComponent<NavMeshAgent>();
		moveTarget = GameObject.Find("MoveTarget");
	}

	// Update is called once per frame
	void Update () {
		if (agent.pathPending) {
			return;
		}

		if (agent.remainingDistance <= agent.stoppingDistance) {
			if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f) {
				moveTarget.GetComponent<MeshRenderer>().enabled = false;
			}
		}
	}

	public void SetTarget(Vector3 v) {
		agent.SetDestination(v);
	}
}
