﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PickScript : MonoBehaviour, IPointerClickHandler {
	private HumanPlayerScript humanPlayerScript;
	private GameObject moveTarget;

	// Use this for initialization
	void Start () {
		var humanPlayer = GameObject.FindGameObjectWithTag("HumanPlayer");
		humanPlayerScript = humanPlayer.GetComponent<HumanPlayerScript>();

		moveTarget = GameObject.Find("MoveTarget");
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Right) {
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			if (Physics.Raycast(ray, out hit)) {
				foreach (var selected in humanPlayerScript.Selected) {
					var movement = selected.GetComponent<MovementScript>();	
					
					if (movement == null) {
						return;
					}
					
					movement.SetTarget(hit.point);

					// TODO: Extract this
					moveTarget.transform.position = new Vector3(hit.point.x, moveTarget.transform.position.y, hit.point.z);
					moveTarget.GetComponent<MeshRenderer>().enabled = true;
				}
			}
		}
	}

	#endregion
}
