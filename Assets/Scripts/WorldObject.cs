using System;
using UnityEngine;

[Serializable]
public class WorldObject
{
	[Header("WorldObject")]
	[SerializeField]
	private int _life = 0;

	[SerializeField]
	private int _armor = 0;

	public int Life {
		get {
			return _life;
		}
		set {
			_life = value;
		}
	}

	public int Armor {
		get {
			return _armor;
		}
		set {
			_armor = value;
		}
	}
}