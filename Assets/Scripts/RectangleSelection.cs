﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class RectangleSelection : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {
	private Vector2 startPosition;
	private Vector3 endPosition;

	private bool selecting = false;

	public RectTransform panel;

	// Use this for initialization
	void Start () {
	
	}

	void Update() {
		if (!selecting) {
			return;
		}

		DrawRectangle();
	}

	/// <summary>
	/// Draws the selection rectangle on the screen.
	/// </summary>
	void DrawRectangle() {
		Vector2 size = Vector2.zero;
		Vector2 position = Vector2.zero;

		if (endPosition.x > startPosition.x) {
			size.x = endPosition.x - startPosition.x;
			position.x = startPosition.x;
		} else {
			size.x = startPosition.x - endPosition.x;
			position.x = endPosition.x;
		}

		if (startPosition.y > endPosition.y) {
			// Select LTR
			size.y = startPosition.y - endPosition.y;
			position.y = startPosition.y - size.y;
		} else {
			// Select RTL
			size.y = endPosition.y - startPosition.y;
			position.y = startPosition.y;
		}
		panel.position = position;
		panel.sizeDelta = size;
	}

	/// <summary>
	/// Uses the hover select feedback to highlight units within the rectangle.
	/// </summary>
	void MarkUnits() {
		var units = GameObject.FindGameObjectsWithTag("Unit");
		Rect rect = new Rect(panel.position, panel.sizeDelta);

		foreach (var unit in units) {
			Vector2 screenPosition = Camera.main.WorldToScreenPoint(unit.transform.position);

			var selection = unit.GetComponent<SelectionScript>();
			selection.HoverSelect(rect.Contains(screenPosition));
		}
	}

	/// <summary>
	/// Selects units within the rectangle.
	/// </summary>
	void SelectUnits() {
		var units = GameObject.FindGameObjectsWithTag("Unit");
		Rect rect = new Rect(panel.position, panel.sizeDelta);

		foreach (var unit in units) {
			Vector2 screenPosition = Camera.main.WorldToScreenPoint(unit.transform.position);

			// unit is not inside the panel.
			var withinRectangle  = rect.Contains(screenPosition);
			var selection = unit.GetComponent<SelectionScript>();

			if (withinRectangle) {
				selection.Select();
			}

			selection.HoverSelect(false);
		}
	}

	#region IPointerDownHandler implementation

	public void OnPointerDown(PointerEventData eventData) {
		startPosition = eventData.position;
		endPosition = eventData.position;
	}

	#endregion

	#region IPointerUpHandler implementation

	public void OnPointerUp(PointerEventData eventData) {
		SelectUnits();

		selecting = false;
		panel.sizeDelta = Vector2.zero;
	}

	#endregion
		
	#region IDragHandler implementation

	public void OnDrag(PointerEventData eventData) {
		MarkUnits();

		selecting = true;
		endPosition = eventData.position;
	}
		
	#endregion
}
