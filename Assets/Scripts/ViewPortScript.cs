﻿using UnityEngine;
using System.Collections;

public class ViewPortScript : MonoBehaviour {
	public GameObject top;
	public GameObject left;
	public GameObject right;
	public GameObject bottom;
	
	public void UpdateBounds (Vector3[] bounds) {
		Vector3 bottomLeft = bounds[0];
		Vector3 topLeft = bounds[1];
		Vector3 topRight = bounds[2];
		Vector3 bottomRight = bounds[3];
		
		var lineTop = top.GetComponent<LineRenderer>();
		var lineLeft = left.GetComponent<LineRenderer>();
		var lineRight = right.GetComponent<LineRenderer>();
		var lineBottom = bottom.GetComponent<LineRenderer>();
		lineTop.SetPosition(0, topLeft);
		lineTop.SetPosition(1, topRight);
		
		lineLeft.SetPosition(0, topLeft);
		lineLeft.SetPosition(1, bottomLeft);
		
		lineRight.SetPosition(0, topRight);
		lineRight.SetPosition(1, bottomRight);
		
		lineBottom.SetPosition(0, bottomLeft);
		lineBottom.SetPosition(1, bottomRight);	
	}
}
