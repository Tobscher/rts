﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[AddComponentMenu("Camera-Control/CameraKeys")]
public class CameraKeys : MonoBehaviour {
	private TopDownCamera topDownCamera;

	public KeyCode FastScrollKey = KeyCode.LeftShift;
	public KeyCode FastScrollKeyAlt = KeyCode.RightShift;

	void Start() {
		topDownCamera = gameObject.GetComponent<TopDownCamera>();
	}

	// Update is called once per frame
	void Update () {
		bool boost = false;

		if (Input.GetKey(FastScrollKey) || Input.GetKey(FastScrollKeyAlt)) {
			boost = true;
		}

		var direction = MoveCameraDirection.None;

		var h = Input.GetAxisRaw("Horizontal");
		if (Mathf.Abs(h) > 0.001f) {
			if (h > 0f) {
				direction |= MoveCameraDirection.Right;
			} else {
				direction |= MoveCameraDirection.Left;
			}
		}
		
		h = Input.GetAxisRaw("Vertical");
		if (Mathf.Abs(h) > 0.001f) {
			if (h > 0f) {
				direction |= MoveCameraDirection.Top;
			} else {
				direction |= MoveCameraDirection.Bottom;
			}
		}
		
		topDownCamera.MoveCamera(direction, boost);
	}
}
