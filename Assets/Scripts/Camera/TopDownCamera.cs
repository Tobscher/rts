﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[AddComponentMenu("Camera-Control/TopDownCamera")]
[RequireComponent(typeof(CameraKeys))]
[RequireComponent(typeof(CameraMouse))]
public class TopDownCamera : MonoBehaviour {
	[Header("Camera")]
	public float ScrollSpeed = 25;
	public float Smoothing = 5f;
	public GameObject viewPort;
	public ViewPortScript viewPortScript;
	public float verticalAxisMultiplier = 1.5f;

	private Vector3 origin;
	private Vector3 destination;
	private Constraints constraints;

	public void Start() {
		origin = transform.position;
		destination = origin;

		viewPortScript = viewPort.GetComponent<ViewPortScript>();
		constraints = GetComponent<Constraints>();
		constraints.UpdateConstraints();

		UpdateBounds();
	}

	public void Update() {
		if (destination != origin) {
			transform.position = constraints.Clamp(Vector3.Lerp(origin, destination, Time.deltaTime * Smoothing));
			
			Vector3[] bounds = getBounds();
			viewPortScript.UpdateBounds(bounds);

			origin = transform.position;
			destination = transform.position;
		}
	}

	public void MoveCamera(MoveCameraDirection direction, bool boost = false) {
		Vector3 movement = Vector3.zero;

		var speed = ScrollSpeed;
		if (boost) {
			speed = speed * 2;
		}

		// horizontal camera movement
		if ((direction & MoveCameraDirection.Left) == MoveCameraDirection.Left) {
			movement.x -= speed;
		} else if ((direction & MoveCameraDirection.Right) == MoveCameraDirection.Right) {
			movement.x += speed;
		}
		
		// vertical camera movement
		if ((direction & MoveCameraDirection.Top) == MoveCameraDirection.Top) {
			movement.z += speed * verticalAxisMultiplier;
		} else if ((direction & MoveCameraDirection.Bottom) == MoveCameraDirection.Bottom) {
			movement.z -= speed * verticalAxisMultiplier;
		}
		
		movement = transform.TransformDirection(movement);
		movement.y = 0;

		destination.x += movement.x;
		destination.y += movement.y;
		destination.z += movement.z;
	}

	public void MoveTo(Vector3 v) {
		transform.position = constraints.Clamp(v);
		origin = transform.position;
		destination = transform.position;

		UpdateBounds();
	}

	public void UpdateBounds() {
		Vector3[] bounds = getBounds();
		viewPortScript.UpdateBounds(bounds);
	}

	private Vector3[] getBounds() {
		var bounds = new Vector3[4];
		Camera camera = Camera.main;

		Ray bottomLeftRay = camera.ViewportPointToRay(new Vector3(0, 0.2f, 0));
		Ray topLeftRay = camera.ViewportPointToRay(new Vector3(0, 1, 0));
		Ray topRightRay = camera.ViewportPointToRay(new Vector3(1, 1, 0));
		Ray bottomRightRay = camera.ViewportPointToRay(new Vector3(1, 0.2f, 0));

		Vector3 bottomLeft = Vector3.zero;
		Vector3 topLeft = Vector3.zero;
		Vector3 topRight = Vector3.zero;
		Vector3 bottomRight = Vector3.zero;
		
		int layerMask = ~(1 << 9);
		RaycastHit hit;
		if (Physics.Raycast (bottomLeftRay, out hit, 100.0f, layerMask)) {
			bottomLeft = hit.point;
			bottomLeft.y = 1.0f;
			bounds[0] = bottomLeft;
		}
		
		if (Physics.Raycast (topLeftRay, out hit, 100.0f, layerMask)) {
			topLeft = hit.point;
			topLeft.y = 1.0f;
			bounds[1] = topLeft;
		}
		
		if (Physics.Raycast (topRightRay, out hit, 100.0f, layerMask)) {
			topRight = hit.point;
			topRight.y = 1.0f;
			bounds[2] = topRight;
		}
		
		if (Physics.Raycast (bottomRightRay, out hit, 100.0f, layerMask)) {
			bottomRight = hit.point;
			bottomRight.y = 1.0f;
			bounds[3] = bottomRight;
		}

		return bounds;
	}
}