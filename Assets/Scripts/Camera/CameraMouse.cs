﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[AddComponentMenu("Camera-Control/CameraMouse")]
public class CameraMouse : MonoBehaviour {
	[Header("Camera")]
	public float ScrollWidth = 5;

	private TopDownCamera topDownCamera;
	
	void Start() {
		topDownCamera = gameObject.GetComponent<TopDownCamera>();
	}

	// Update is called once per frame
	void Update () {
		Rect screenRect = new Rect(0,0, Screen.width, Screen.height);
		if (!screenRect.Contains(Input.mousePosition))
			return;

		var direction = MoveCameraDirection.None;

		float xpos = Input.mousePosition.x;
		float ypos = Input.mousePosition.y;

		if (xpos >= 0 && xpos < ScrollWidth) {
			direction |= MoveCameraDirection.Left;
		} else if (xpos <= Screen.width && xpos > Screen.width - ScrollWidth) {
			direction |= MoveCameraDirection.Right;
		}

		//vertical camera movement
		if (ypos >= 0 && ypos < ScrollWidth) {
			direction |= MoveCameraDirection.Bottom;
		} else if (ypos <= Screen.height && ypos > Screen.height - ScrollWidth) {
			direction |= MoveCameraDirection.Top;
		}

		topDownCamera.MoveCamera(direction);
	}
}
