using System;

[Flags]
public enum MoveCameraDirection
{
	None = 0,
	Top = 1,
	Left = 2,
	Right = 4,
	Bottom = 8,
	TopLeft = Top | Left,
	TopRight = Top | Right,
	BottomLeft = Bottom | Left,
	BottomRight = Bottom | Right
}