﻿using UnityEngine;
using System.Collections;

public class Constraints : MonoBehaviour {
	private float maxWidth;
	private float maxHeightTop;
	private float maxHeightBottom;

	private float clampTop;
	private float clampLeft;
	private float clampRight;
	private float clampBottom;

	public Vector3 Clamp(Vector3 p) {
		var v = new Vector3(Mathf.Clamp(p.x, clampLeft, clampRight), p.y, Mathf.Clamp(p.z, clampBottom, clampTop));
		return v;
	}

	public void UpdateConstraints() {
		Camera camera = Camera.main;
		
		Ray topLeftRay = camera.ViewportPointToRay(new Vector3(0, 1, 0));
		Ray bottomRightRay = camera.ViewportPointToRay(new Vector3(1, 0.2f, 0));
		
		Vector3 top = Vector3.zero;
		Vector3 bottom = Vector3.zero;
		
		int layerMask = ~(1 << 9);
		RaycastHit hit;
		if (Physics.Raycast (topLeftRay, out hit, 100.0f, layerMask)) {
			top = hit.point;
		}
		
		if (Physics.Raycast (bottomRightRay, out hit, 100.0f, layerMask)) {
			bottom = hit.point;
		}
		
		maxWidth = Mathf.Abs(top.x * 2);
		maxHeightTop = Mathf.Abs(top.z);
		maxHeightBottom = Mathf.Abs(bottom.z);

		clampTop = 64.0f - (maxHeightTop) + (camera.transform.position.z);
		clampLeft = -(64.0f - (maxWidth / 2.0f));
		clampRight = 64.0f - (maxWidth / 2.0f);
		clampBottom = -(64.0f - (maxHeightBottom)) + (camera.transform.position.z);
	}
}
