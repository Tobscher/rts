﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HumanPlayerScript : MonoBehaviour {
	private List<GameObject> selected = new List<GameObject>();
	private GameObject selectedObjectUI;

	public IEnumerable<GameObject> Selected {
		get {
			return selected;
		}
	}

	public void Start() {
		selectedObjectUI = GameObject.Find("SelectedObjectText");
	}

	public void SetSelect(GameObject toSelect, bool select) {
		GameObject selector = null;

		foreach (Transform child in toSelect.transform) {
			if (child.CompareTag("Selector")) {
				selector = child.gameObject;
			}
		}

		if (selector == null) {
			return;
		}

		selector.SetActive(select);
	}

	public void UnselectAll() {
		for (int i = 0; i < selected.Count; i++) {
			Unselect(selected[i]);
		}
		selected.Clear();
	}

	public void Unselect(GameObject gameObject) {
		SetSelect(gameObject, false);
		selected.Remove(gameObject);
		UpdateUI();
	}
	
	public void Select(GameObject gameObject) {
		UnselectAll();
		SetSelect(gameObject, true);
		selected.Add(gameObject);
		UpdateUI(gameObject);
	}

	public IList<GameObject> GetSelected() {
		return selected;
	}

	private void UpdateUI(GameObject gameObject = null) {
		if (gameObject == null) {
			selectedObjectUI.GetComponent<Text>().text = string.Empty;
		} else {
			selectedObjectUI.GetComponent<Text>().text = gameObject.name;
		}
	}
}
