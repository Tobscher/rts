﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class SelectionScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
	private GameObject humanPlayer;
	private GameObject selector;

	// Use this for initialization
	void Start () {
		humanPlayer = GameObject.FindGameObjectWithTag("HumanPlayer");
	}

	public void HoverSelect(bool state) {
		GameObject selector = null;
		
		foreach (Transform child in transform) {
			if (child.CompareTag("Selector-Hover")) {
				selector = child.gameObject;
			}
		}
		
		if (selector == null) {
			return;
		}
		
		selector.SetActive(state);
	}

	public void Select()
	{
		humanPlayer.GetComponent<HumanPlayerScript>().Select(gameObject);
	}

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData) {
		HoverSelect(true);
	}

	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData) {
		HoverSelect(false);
	}

	#endregion

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData) {
		Select();
	}

	#endregion
}
