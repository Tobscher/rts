﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MiniMapMovement : MonoBehaviour, IPointerDownHandler, IDragHandler {
	private HumanPlayerScript humanPlayerScript;
	public float cameraOffset = 20.0f;

	TopDownCamera topDown;

	// Use this for initialization
	void Start () {
		topDown = Camera.main.GetComponent<TopDownCamera>();

		var humanPlayer = GameObject.FindGameObjectWithTag("HumanPlayer");
		humanPlayerScript = humanPlayer.GetComponent<HumanPlayerScript>();
	}

	private Vector3 getPosition(float y, float offset) {
		var scaleY = Screen.height / 800.0f;

		var f = 128.0f;
		var t = gameObject.GetComponent<RectTransform>();
		var parent = transform.parent.GetComponent<RectTransform>();
		Vector2 size = new Vector2(t.rect.width * scaleY, t.rect.height * scaleY);
		var xfactor = f / size.x;
		var zfactor = f / size.y;

		var xs = Input.mousePosition.x - (((parent.rect.width - t.rect.width) * scaleY) / 2);
		var zs = Input.mousePosition.y - (((parent.rect.height - t.rect.height) * scaleY) / 2);
		var x = (xs) * xfactor;
		var z = (zs) * zfactor;

		var v = new Vector3(x - (f/2.0f), y, z - (f/2.0f) - offset);

		return v;
	}
	
	// Moves camera to the clicked position on mini map
	public void MoveCamera () {
		topDown.MoveTo(getPosition(Camera.main.transform.position.y, cameraOffset));
	}

	// Moves all selected units to the clicked position on mini map
	public void MoveSelected() {
		foreach (var selected in humanPlayerScript.Selected) {
			var movement = selected.GetComponent<MovementScript>();	

			if (movement == null) {
				return;
			}

			// Raycast to determine highest point
			movement.SetTarget(getPosition(0.0f, 0.0f));
		}
	}

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Left) {
			MoveCamera();
			return;
		}

		if (eventData.button == PointerEventData.InputButton.Right) {
			MoveSelected();
			return;
		}
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		MoveCamera();
	}

	#endregion
}
