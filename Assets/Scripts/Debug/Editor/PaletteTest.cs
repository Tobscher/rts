﻿using UnityEngine;
using System.Collections;
using NUnit.Framework;

[TestFixture]
public class PaletteTest {
	[Test]
	public void GetColorTopLeft() {
		var expected = new Vector2(8.0f / Palette.Width, 232.0f / Palette.Height);
		var actual = Palette.GetColor(Palette.Color.Red, Palette.Weight.Base);

		Assert.AreEqual(expected, actual);
	}

	[Test]
	public void GetColorBottomLeft() {
		var expected = new Vector2(8.0f / Palette.Width, 8.0f / Palette.Height);
		var actual = Palette.GetColor(Palette.Color.Red, Palette.Weight.A700);

		Assert.AreEqual(expected, actual);
	}

	[Test]
	public void GetColorRandom() {
		var expected = new Vector2(152.0f / Palette.Width, 136.0f / Palette.Height);
		var actual = Palette.GetColor(Palette.Color.Green, Palette.Weight._500);

		Assert.AreEqual(expected, actual);
	}

	[Test]
	public void GetBlack() {
		var expected = new Vector2(312.0f / Palette.Width, 232.0f / Palette.Height);
		var actual = Palette.GetBlack();

		Assert.AreEqual(expected, actual);
	}

	[Test]
	public void GetWhite() {
		var expected = new Vector2(312.0f / Palette.Width, 216.0f / Palette.Height);
		var actual = Palette.GetWhite();

		Assert.AreEqual(expected, actual);
	}
}